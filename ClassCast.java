package week5.TryAndCatch;
class Vehicle{
    String colour;
    public void colour(String colour){
        System.out.println(this.colour);
    }
    public  void engine(){
        System.out.println("bow bow");
    }


}
class Car extends Vehicle{
    @Override
    public void engine() {
        super.engine();
    }

}

public class ClassCast {
    public static void main(String[] args) {
        Vehicle vehicle=new Vehicle();
        Car car=new Car();

        try{
            Car car2=(Car)vehicle;
            car2.colour("black");
        }catch (ClassCastException c){
            System.out.println(" plz use child class methods only");
        }finally {
            System.out.println("end of the program");
        }


    }
}
